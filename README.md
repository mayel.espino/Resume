# Mayel Espino's Resume
- (510)512-5323
- job@mayel.info
- www.mayelespino.com
- https://www.linkedin.com/in/mayelespino 

## Highlights
* Received 2 applause awards at Symantec.
* Inventor of 6 patent applications and co-inventor of 1 patent.
* My team won the first place in CQES first “hack-a-thon” at PayPal.
* Received the “PayPalian” award for my involvement for the Airlines Project.

## Skill Set
### Programming languanges

``` C - 20 years ```

- Back end servers 
- Device driver to control tape drive 
- Embedded systems 
– Massive file processing 
- image processing libraries 
– Command line utilities.

``` C++ - 16 years ```

- Large-scale Back-end servers 
– Massive file processors for credit card applications. 
- SOAP services.

``` Java - 01 year ```
- Servlets 
- Back end servers- Maven test cases 
– MQ classes.

``` Assembler - 01 years ```
- Embedded systems in college.

``` Python - 04 years```
- Command line utility 
- Server development 
– Messaging/middleware systems 
– Monitoring tools.

```DJango - 01 year```
- REST server 
- Command Line REST client 
– In-house application for storing service monitor data for reporting engine 
– Reporting engine.

```Ruby - 06 months ``` 
- Received training from Puppet developers. 
- Developed puppet modules. 
- Developed Chef recipes and tests.

### Scripting languages

```Bash Scripting - 07 years```
- Administrative task automation 
- Environment set up and management 
– System Monitoring.

``` Perl - 04 years``` 

- Unix process management 
- Service monitoring 
- File processing.

### Infrastructure

``` Infrastructure management and deployment automation - 04 years``` 

- Developed and improved on a variety of tools, both in house and off the shelf, to enable the Release Engineering team deploy, configure and manage the entire company’s software stack.

```Cloud computing - 1 year``` 

- Provisioned and managed nodes in both AWS and OpenStack. Created and managed security groups. Used the web interfaces and command line tools for both.

```Chef - 06 months``` 

- Wrote chef recipes, chef kitchen tests, used kitchen knife for chef server operations and node management.

```Haproxyand Squid - 04 months``` 

- Manage, update haproxy. Created Chef recipes to produce the haproxy configuration.

```Consul - 02 months``` 

- Added service discovery to haproxy.

``` DataDog - 02 months``` 

- Created DD agent scripts to monitor services, also created dashboards and alerts.

```Puppet - 08 months``` 

- Received training from Puppet developers 
– Developed puppet modules 
- Tutorial examples 
- puppet master 
– Mcollective.

``` Oncall support - 04 years``` 
- Provided second level support and tools for the DevOps and Batch Ops teams at PayPal and for SRE team at Ooyala.

``` Continuous integration - 04 years``` 

- Created continuous integration jobs, code coverage jobs, Maven test execution jobs.

``` Source control - 11 years``` 

- Used for source control of work projects.

``` Mysql - 01 year ``` 

- Installation, schema design and performance tuning.

``` Oracle/Sybase - 07 years``` 
- Schema design and performance tuning.

``` SQL/PSQL - 09 years ```
- Report generation - PSQL programs for data pre-processing.

### Operating Systems
``` Linux - 12 years``` 
- Install and developed open source and in-house applications - Systems Administration and management – Process and service monitoring.

``` Unix - 11 years ``` 
- Systems Administration and management 
– Process and service monitoring 
- Ported C/C++ code on to various variations of Unix.

``` MVS - 04 years ``` 
- Applications C, REX, and proprietary languages.

``` Windows - 10 years``` 
- Software Development and management.

### Web
```HTM - 07 years```

- Developed a variety of web pages for internal tools.

``` PHP - 01 year ``` 

- Developed in-house applications for MCI test tool.

```JavaScript - 06 months``` 

- Developed Service monitor web page using JavaScript to read files and execute REST commands to dynamically build the status web page.

``` Mark up languages - 04 years ``` 

- XML for configuration data 
- Used Json in conjunction to MongoDB to describe our deployment application’s configuration data.

### Middleware
``` MQ Series - 01 year ``` 

- Developed, maintained and ported code for messaging applications, which utilized MQ-Series API.

``` WebSphere - 01 years ``` 

- Developed messaging applications for complex messaging and message transformation.

``` Tuxedo - 01 year ```

- Developed business logic for tuxedo servers, using tuxedo’s API.

### Other
``` Project management - 07 years ``` 

- Lead large teams, up to 9 engineers, in design and project tracking meetings. Day to day mentoring and project tracking.

``` Embedded Systems - 01 years ``` 

- Developed Embedded systems programs for college. 

``` Internet security (AppScan) - 06 Months``` 
- Maintained and extended in house infrastructure for scanning PayPal’s site for internet security vulnerabilities. The infrastructure was built around AppScan an IBM product.

![Apple inc](images/apple-squarelogo-1485292862952.png)
## Apple 
__April 2016 to December 2017__ 

`Software engineer - AML Infrastructure Engineering` : 
As part of the team I helped managed and maintain a large infrastructure to serve field diagnostic applications for Apple world wide. Highly available, multi-site.
__Release Management__ Deployed code to production and non-production environments. Help coordinate the use of the non-production environments with multiple development teams.
__Triage__ Managed ticket queue. Assigned tickets to developers and other team members. Help Development team in debugging and root cause analysis.
__Crisis Management__ Participated in the on-call rotation for  High priority tickets tickets.

`saltstack - python - bash scripting - Ansible - Cassandra - Hadoop`

![FireEye](images/fireeye-squarelogo-1452294049498.png)
## FireEye
__October 2015 to April 2016__ | Milpitas,California

`FELabs Infrastructure DevOps Team:`

Development of automated tools for monitoring, management and deployment of horizontaly-scaled, highly-available cloud systems. Architecting multi-datacenter monitoring facilities. Building performance and reliability metrics in a dynamic application environment. Integrate with engineering teams to provide expertise and requirements.

`Ansible Tower, Prometheus, ELK Stack, Docker, Python.`

![Ooyala](images/ooyala-squarelogo-1510101523308.png)
## Ooyala
__March 2015 to August 2015__ 

`Site Reliability Engineering (and Infrastructure) Team:`
__Research__ and selecting or extending the right frameworks, tools and technologies for Ooyala's core platform. 
__Continuously improving__ and re-engineering the platform architecture to handle our rapidly growing traffic. 
Designing and implementing clean APIs and protocols for other teams to build features on top of.
__Troubleshooting__ and resolving critical issues with the platform. 
__Guide and support__ product engineering teams in making full use of the platforms and tools the team provides 

`Chef, Ruby, AWS, MetaCloud, Consul, Data Dog, Haproxy, Squid, Jira`

![Symantec](images/symantec-squarelogo-1495123496483.png)
## Symantec
__May 2014 to March 2015__ 

`Mobile Security Customer Response Team:`
 __Investigate and resolve__ customer issues with Symantec’s Mobility Manager server (Mobile security). 
__Held online meetings with customers__ to help them resolve difficult issues. 
__Designed and implemented__ a “HotFix” process for quick delivery of code fixes or patches to customers. 
__Developed__ “Analog”, an application log analysis tool. Created a SymExchange space for CRT support

`Python, Django, PyCharm, SVN, Vagrant, AWS, Monit, celery, Rabitt MQ, messaging.`

![PayPal](images/paypal-squarelogo-1440712027841.png)
## PayPal
__October 2006 to May 2014__

`Infrastructure Team:`
__Developed__ infrastructure for and drove pilot program for the Internet Security team, based on IBM’s AppScan. 
__Developed__ infrastructure for testing messaging infrastructure, called AMQ.

`Stage Reliability Tiger Team:`
Hand picked for __Stage Reliability tiger team__, to tackle:  “the most critical issue affecting productivity at PayPal.
__Designed and developed__ a monitoring tool called “Argus”. Which monitors PayPal Services, 
restarts them and sends metrics data to a backend DB for reporting and analysis.

`Deployment Infrastructure`: 
__Design and developed__ command line interface client/driver for PayPal’s Dispatcher. A Deployment System, built on top of Puppet.

` FinProd:`
__Developed__ for the high visibility integration of Bill me latter to the PayPal website.

` FinSys: `
__Project managed and developer__ code for several large scale, credit card payment processing projects: FDMS integration, AMEX, and Airlines.

`C++, Java, Perl, Python, Ruby, Bash, AppScan (IBM), Jenkins, Jaws, Maven, github, RH Linux, REST API, Oracle DB, Django, Puppet,SOAP, clear case, RH Linux. Project Management.`

![e-trade](images/e-trade-financial-squarelogo.png)
## E*trade
__February 2006 to September 2006__

__Developed__ projects for E*trade advisors. These were Tuxedo services (server code), which interact with internal web pages.

`C++, Tuxedo, Java, Perl & SQL, Linux.`

![Kabira](images/kabira.png)
## Kabira
__August 2005 to January 2006__

__Developed__ code for Kabira’s clients.

`Kabira’s proprietary language C++, Solaris Unix.`

![Verizon](images/verizon-squarelogo-1487720317378.png)
### MCI (Now Verizon Business)
__1996 to 2004__
`Registry Middleware team`:
__Developed__ code for Registry, MCI’s messaging (middleware) infrastructure. A mission critical framework which performed complex messaging,and message transformation for some 200 applications. Registry interconnected application across multiple hardware and OS platforms. 
`BMIT`:
__Managed__ a cross business unit team of testers to plan and execute end-to-end testing.

`C++, IBM’ Unix (AIX), HP’s Unix, Solaris, MVS, Windows and many others. Web-sphere, MQ series, Java Servlets, Insure++, xdbc.`

![IBM](himages/ibm-squarelogo-1502810404854.png)
### EDM / Candle Corporation (Now part of IBM)
__1991 to 1996__

__Selected for the pioneering__ team in Mexico to develop Candle’s Mainframe monitoring  and automation tools.
 Project manager for AF/Operator and AF/Performer automation tools, the largest team in Mexico with 9 engineers reporting to me.

`Candle’s proprietary languages, C++, Rexx, assembler. Project Management.`


## U.A.C.J. 1984 - 1990
B.S. degree in computer engineering, fully accredited in the United States.

## Colorado Technical University 1998 & 1999
C++ and Object Oriented certifications.



## Profesional information
- [Linked-in Profile](https://www.linkedin.com/in/mayelespino/)
- [U.A.C.J](http://www.uacj.mx/Paginas/Default.aspx)
- [My Patents](http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&p=1&u=%2Fnetahtml%2FPTO%2Fsearch-bool.html&r=0&f=S&l=50&TERM1=mayel&FIELD1=INNM&co1=AND&TERM2=&FIELD2=&d=PTXT)

## Personal projects
- [Git Hub Code examples](https://github.com/mayelespino/code)
- [Raspberry PI projects](https://github.com/mayelespino/raspberry-pi)
- [picron](https://youtu.be/hJEf7LCYN3s)
- [El Contrato](https://youtu.be/wOGnehaLklw)